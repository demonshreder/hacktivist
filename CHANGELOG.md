# Changelog

## [0.1.0] - 2023-05-07

### Added

- Started the a fresh django-project
- Added gitignore, LICENSE, CHANGELOG.md, requirements.txt, dockerignore, VERSION
